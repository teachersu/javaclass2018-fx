## 20190223
#### Install e(fx)clipse 
[Guide](https://www.eclipse.org/efxclipse/install.html)

If javaFX is missed on Mac OS, [download the latest java JDK](https://www.oracle.com/technetwork/java/javase/downloads/index.html) version from Oracle.

#### Install JavaFX Scene Builder
<https://gluonhq.com/products/scene-builder/>

Note: Make sure download correct version.